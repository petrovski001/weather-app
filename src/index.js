import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './components/navbar/navbar';
import Loader from './components/spinner/loader';
import Card from './components/card/card';
import Container from './components/container/container';

function App() {
    const URL = "https://api.darksky.net/forecast";
    const API_KEY = "78ab9a8e504eb43da4b72a39b36bf094";
    const CORS_URL = "https://cors-anywhere.herokuapp.com/";
    let lat = '';
    let long = '';
    const [weatherReport, setWeatherReport] = React.useState(null);

    React.useEffect(() => {
        getLocation();
    }, [])

    const getLocation = () => {
        window.navigator.geolocation.getCurrentPosition((succ) => {
            console.log(succ);
            lat = succ.coords.latitude;
            long = succ.coords.longitude;
            getData();
        },
        (err) => {
            console.log(err.message);
        });
    }

    const getData = async () => {
        // fetch data from a url endpoint
        await axios.get(CORS_URL + URL +"/"+ API_KEY + "/" + lat + ","+ long, {
            headers: {
                'Allow-Control-Allow-Origin': '*'
               },
            params: {
                units: 'si'
            }
        }).then((data) => {
            console.log(data.data);
            setWeatherReport(data.data);
        }, (err) => {
            console.log(err);
        });
    }


    if(weatherReport == null) {
        return <div className="container">
            <Loader/>
        </div>
    }
    else {
        return <div>
            <Navbar/>
            <div className="container">
                <Card data = {weatherReport} />
                <Container data = {weatherReport.daily.data}/>
            </div>
        </div>
    }
}


ReactDOM.render(<App/>, document.getElementById('root'));