import React from 'react';
//import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

class Navbar extends React.Component {
    render() {
        return (
            <nav className="navbar sticky-top navbar-dark bg-primary navbar-expand-md py-md-2">
            <div className="container">
                <a className="navbar-brand" href="/">Weather channel</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="navbar-collapse collapse" id="navbarNav">
                    <ul className="navbar-nav">

                    </ul>
                </div>
            </div>
        </nav>
        );
    }
}

//ReactDOM.render(<Navbar/>, document.getElementById('nav'));

export default Navbar;