import React from 'react';
import './item.css';

class Item extends React.Component {

    getDayOfTheWeek = () => {
        let days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        let day = new Date(this.props.data.time * 1000);
        return days[day.getDay()];
    }

    render() {
        return (
            <div className="item item-2">
                <div id="first" className="row">
                    <p>{this.getDayOfTheWeek()}</p>
                </div>
                <div id="middle" className="row">
                    <img src={"/icons/" + this.props.data.icon + ".png"} alt={this.props.data.summary}></img>
                </div>
                <div id="end" className="row">
                    <p id='high'>{Math.round(this.props.data.temperatureHigh)}°</p>
                    <p id='low'>{Math.round(this.props.data.temperatureLow)}°</p>
                </div>
            </div>
        );
    }
}

export default Item;