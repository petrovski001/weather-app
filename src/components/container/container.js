import React from 'react';
import Item from './items/item';

class Container extends React.Component {
   //props.data

   render() {
      return (
         <div className="container-fluid">
            {
               this.props.data.map((day) => {
                  return (
                     <Item key={day.time} data={day}/>
                  );
               })
            }
         </div>
      );
   }
}

export default Container;