import React from 'react';
import './hourly.css';

class Hourly extends React.Component {
    getTime = () => {
        let date = new Date(this.props.data.time * 1000);
        return date.getHours() + ":00";
    }

    render() {
        return (
            <div className="hourly">
                <div id="up" className="row">
                    <p>{this.getTime()}</p>
                </div>
                <div id="mid" className="row">
                    <img src={"/icons/"+this.props.data.icon+".png"} alt={this.props.data.summary}></img>
                </div>
                <div id="down" className="row">
                    <p>{Math.round(this.props.data.temperature)}°</p>
                </div>
            </div>
        );
    }
}

export default Hourly;