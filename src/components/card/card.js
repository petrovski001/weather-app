import React from 'react';
import './card.css'
import Hourly from './hourly/hourly';

class Card extends React.Component {
    constructor(props) {
        super(props);
        this.state = {date: null};
    }

    componentWillMount() {
         let arr = this.props.data.timezone.split("/");
         this.props.data.timezone = arr[1];
         if(arr[1].includes("_"))
             this.props.data.timezone = arr[1].replace("_", " ");

         this.props.data.currently.windSpeed = Math.floor(this.props.data.currently.windSpeed);
         let humidity = this.props.data.currently.humidity.toString().split(".");
         this.props.data.currently.humidity = humidity[1];
    }

    getWindDirection = () => {
        let degree = this.props.data.currently.windBearing;
        if (degree>337.5) return 'N';
        if (degree>292.5) return 'NW';
        if(degree>247.5) return 'W';
        if(degree>202.5) return 'SW';
        if(degree>157.5) return 'S';
        if(degree>122.5) return 'SE';
        if(degree>67.5) return 'E';
        if(degree>22.5){return 'NE';}
        return 'N';
    }

    render(){ 
        return (
            <div className="card card-3">
                <div id="title" className="row">
                    <div className="col-md-7">
                        <h2>{this.props.data.timezone}</h2>
                        <p><strong>{this.props.data.currently.summary}</strong></p>
                    </div>
                    <div className="col-md-5">
                        <p>{this.getWindDirection()} {this.props.data.currently.windSpeed + "km/h"}</p>
                        <span id="humidity">Humidity {this.props.data.currently.humidity + "%"}</span>
                    </div>
                </div>
                <div id="body" className="row">
                    <div className="col-md-6">
                        <img src={"/icons/" + this.props.data.currently.icon + ".png"} alt={this.props.data.currently.summary}></img>
                    </div>
                    <div className="col-md-6">
                        <p>{Math.floor(this.props.data.currently.temperature)+ '°'}</p>
                    </div>
                </div>
                <div id="footer" className="row">
                    {
                        this.props.data.hourly.data.slice(0,4).map((hour) => {
                            return (<Hourly key={hour.time} data={hour}/>);
                        })
                    }
                </div>
            </div>
        );
    }
}

export default Card;